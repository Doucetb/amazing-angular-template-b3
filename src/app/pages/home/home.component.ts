import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  resNantes = [];
  constructor(private http: HttpClient) { }
  apiNantes(){
    const obsNantes = this.http.get('https://data.nantesmetropole.fr/api/records/1.0/search/?dataset=244400404_nombre-annuel-deces-nantes&rows=72&sort=annee&facet=annee&facet=sexe');
    obsNantes.subscribe(httpNantes => {
      httpNantes['records'].forEach(az => this.resNantes.push(az.fields));
    });
  }
  ngOnInit() {
    this.apiNantes();
  }

}
