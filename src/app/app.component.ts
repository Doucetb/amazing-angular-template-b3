import { Component } from '@angular/core';
import '../../node_modules/chart.js/dist/Chart.bundle.min.js';
import '../../node_modules/chart.js/dist/Chart.min.js';
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'Amazing Angular Project';
}
